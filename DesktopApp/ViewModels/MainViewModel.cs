﻿using System;
using System.Text;
using System.Windows;
using CommonServiceLocator;
using DesktopApp.Helpers;
using DesktopApp.View.DialogView;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using MaterialDesignThemes.Wpf;
using System.Windows.Controls;
using Model.Common;

namespace DesktopApp.ViewModels
{
    public class MainViewModel : ViewModelBaseWithCommand
    {
        private Address address;
        private Phone phones;
        private MedicalCard card;
        private readonly Person person;

        public MainViewModel()
        {
            Messenger.Default.Register<NotificationMessage<Address>>(this, m =>
            {
                if (m.Notification == "Address")
                    if (m.Content != null)
                        this.address = m.Content;
            });

            Messenger.Default.Register<NotificationMessage<Phone>>(this, m =>
            {
                if (m.Notification == "Phone")
                    if (m.Content != null)
                        this.phones = m.Content;
            });

            Messenger.Default.Register<NotificationMessage<MedicalCard>>(this, m =>
            {
                if (m.Notification == "MedicalCard")
                    if (m.Content != null)
                        this.card = m.Content;
            });

            this.person = new Person();
        }
        
        private RelayCommand<string> openDialogCommand;
        
        public RelayCommand<string> OpenDialogCommand
        {
            get
            {
                return this.openDialogCommand ?? (this.openDialogCommand = new RelayCommand<string>(ShowDialogHandler));
            }
        }

        public string FirstName
        {
            get => this.person.FirstName;
            set => this.person.FirstName = value;
        }

        public string LastName
        {
            get => this.person.LastName;
            set => this.person.LastName = value;
        }

        public string MiddleName
        {
            get => this.person.MiddleName;
            set => this.person.MiddleName = value;
        }
        
        public DateTime Birthday
        {
            get => this.person.Birthday;
            set => this.person.Birthday = value;
        }

        public Gender Gender
        {
            get => this.person.Gender;
            set => this.person.Gender = value;
        }

        private async void ShowDialogHandler(string s)
        {
            if (string.IsNullOrEmpty(s)) return;

            UserControl view;
            switch (s)
            {
                case "Address":
                    view = ServiceLocator.Current.GetInstance<AddressDialogView>();
                    break;
                case "Phone":
                    view = ServiceLocator.Current.GetInstance<PhoneDialogView>();
                    break;
                case "Medical":
                    view = ServiceLocator.Current.GetInstance<MedicalDialogView>();
                    break;
                default:
                    view = null;
                    break;
            }

            if (view == null) return;

            var result = await DialogHost.Show(view, "RootDialog");
        }

        private RelayCommand createCardCommand;

        /// <summary>
        /// Получение команды для создания медецинской карты
        /// </summary>
        public RelayCommand CreateCardCommand
        {
            get { return this.createCardCommand ?? (this.createCardCommand = new RelayCommand(() =>
            {
                var info = new Info()
                {
                    Person = this.person,
                    Address = this.address ?? (this.address = new Address()),
                    Phones = this.phones ?? (this.phones = new Phone()),
                    Card = this.card ?? (this.card = new MedicalCard())
                };

                var sb = new StringBuilder();

                sb.Append($"Полное имя ребенка: {info.Person.FirstName} {info.Person.MiddleName ?? ""} {info.Person.LastName}");
                sb.Append(Environment.NewLine);

                var day = info.Person.Birthday.Day;
                var year = info.Person.Birthday.Year;
                string month = GetMonthName(info.Person.Birthday.Month);

                sb.Append($"Дата рождения: {day} {month} {year} года");
                sb.Append(Environment.NewLine);

                sb.Append("Адрес проживания:");
                sb.Append(Environment.NewLine);

                sb.Append($"г. {info.Address.City},");
                sb.Append(Environment.NewLine);
                sb.Append($"ул. {info.Address.Street}, " +
                          $"д. {info.Address.House}{info.Address.Suffix ?? ""}, " +
                          $"кв. {info.Address.Appartment}");

                sb.Append(Environment.NewLine);
                sb.Append($"Мобильный телефон: {info.Phones.Mobile}");
                sb.Append(Environment.NewLine);
                sb.Append($"Домашний телефон: {info.Phones.Home ?? new string('_', 20)}");
                sb.Append(Environment.NewLine);

                MessageBox.Show(sb.ToString());
            })); }
        }

        private string GetMonthName(int month)
        {
            switch (month)
            {
                case 1:
                    return "января";
                case 2:
                    return "февраля";
                case 3:
                    return "марта";
                case 4:
                    return "апреля";
                case 5:
                    return "мая";
                case 6:
                    return "июня";
                case 7:
                    return "июля";
                case 8:
                    return "августа";
                case 9:
                    return "сентября";
                case 10:
                    return "октября";
                case 11:
                    return "ноября";
                case 12:
                    return "декабря";
                default:
                    return "";
            }
        }
    }
}