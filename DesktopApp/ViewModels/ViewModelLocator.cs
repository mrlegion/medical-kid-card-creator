﻿using CommonServiceLocator;
using DesktopApp.View.DialogView;
using DesktopApp.ViewModels.Dialog;
using GalaSoft.MvvmLight.Ioc;

namespace DesktopApp.ViewModels
{
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<AddressDialogViewModel>();
            SimpleIoc.Default.Register<PhoneDialogViewModel>();

            // dialogs
            SimpleIoc.Default.Register<AddressDialogView>();
            SimpleIoc.Default.Register<AddressDialogViewModel>();

            SimpleIoc.Default.Register<PhoneDialogViewModel>();
            SimpleIoc.Default.Register<PhoneDialogView>();

            SimpleIoc.Default.Register<MedicalDialogView>();
            SimpleIoc.Default.Register<MedicalDialogViewModel>();
        }

        public MainViewModel Main
        {
            get { return ServiceLocator.Current.GetInstance<MainViewModel>(); }
        }

        public AddressDialogViewModel AddressDialog
        {
            get { return ServiceLocator.Current.GetInstance<AddressDialogViewModel>(); }
        }

        public PhoneDialogViewModel PhoneDialog
        {
            get { return ServiceLocator.Current.GetInstance<PhoneDialogViewModel>(); }
        }

        public MedicalDialogViewModel MedicalDialog
        {
            get { return ServiceLocator.Current.GetInstance<MedicalDialogViewModel>(); }
        }
    }
}