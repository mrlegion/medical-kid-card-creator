﻿using DesktopApp.Helpers;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Model.Common;

namespace DesktopApp.ViewModels.Dialog
{
    public class AddressDialogViewModel : ViewModelBaseWithCommand
    {
        private Address address;

        public AddressDialogViewModel()
        {
            this.address = new Address();
        }

        public string City
        {
            get { return this.address.City; }
            set { this.address.City = value; }
        }

        public string Street
        {
            get { return this.address.Street; }
            set { this.address.Street = value; }
        }
        
        public string House
        {
            get { return this.address.House; }
            set { this.address.House = value; }
        }
        
        public string Suffix
        {
            get { return this.address.Suffix; }
            set { this.address.Suffix = value; }
        }
        
        public string Appartment
        {
            get { return this.address.Appartment; }
            set { this.address.Appartment = value; }
        }


        /// <summary>
        /// Private <see cref="sendAddressCommand"/> name
        /// </summary>
        private RelayCommand sendAddressCommand;

        /// <summary>
        /// Get command for SendAddressCommand
        /// </summary>
        public RelayCommand SendAddressCommand
        {
            get { return this.sendAddressCommand ?? (this.sendAddressCommand = new RelayCommand(() => 
            {
                Messenger.Default.Send<NotificationMessage<Address>>(
                    new NotificationMessage<Address>(this.address ?? (this.address = new Address()), "Address"));
            })); }
        }

    }
}   