﻿using DesktopApp.Helpers;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Model.Common;

namespace DesktopApp.ViewModels.Dialog
{
    public class PhoneDialogViewModel : ViewModelBaseWithCommand
    {
        private Phone phone;

        public PhoneDialogViewModel()
        {
            this.phone = new Phone();
        }

        public string Home
        {
            get => this.phone.Home;
            set => this.phone.Home = value;
        }

        public string Mobile
        {
            get => this.phone.Mobile;
            set => this.phone.Mobile = value;
        }

        private RelayCommand sendPhoneCommand;
        
        public RelayCommand SendPhoneCommand
        {
            get { return this.sendPhoneCommand ?? (this.sendPhoneCommand = new RelayCommand(() => 
            {
                Messenger.Default.Send<NotificationMessage<Phone>>(
                    new NotificationMessage<Phone>(this.phone ?? (this.phone = new Phone()), "Phone"));
            })); }
        }

    }
}