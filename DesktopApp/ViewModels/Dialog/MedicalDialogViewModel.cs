﻿using DesktopApp.Helpers;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Model.Common;

namespace DesktopApp.ViewModels.Dialog
{
    public class MedicalDialogViewModel : ViewModelBaseWithCommand
    {
        private readonly MedicalCard card;

        public MedicalDialogViewModel()
        {
            this.card = new MedicalCard();
        }
        
        /// <summary>
        /// Получение или установка номера медицинской карты
        /// </summary>
        public int NumberCard
        {
            get => this.card.NumberCard;
            set => this.card.NumberCard = value;
        }
        
        /// <summary>
        /// Получение или установка номера участка
        /// </summary>
        public int KidArea
        {
            get => this.card.KidArea;
            set => this.card.KidArea = value;
        }
        
        /// <summary>
        /// Получение или установка номера группы крови
        /// </summary>
        public BloodGroup BloodGroup
        {
            get => this.card.BloodGroup;
            set => this.card.BloodGroup = value;
        }
        
        /// <summary>
        /// Получение или установка резуса-фактора
        /// </summary>
        public Rhesus RhesusFactor
        {
            get => this.card.RhesusFactor;
            set => this.card.RhesusFactor = value;
        }
        
        /// <summary>
        /// Получение или установка номера страхового полиса
        /// </summary>
        public string NumberInsuranceCertificate
        {
            get => this.card.NumberInsuranceCertificate;
            set => this.card.NumberInsuranceCertificate = value;
        }

        private RelayCommand sendMedicalCardCommand;

        public RelayCommand SendMedicalCardCommand
        {
            get { return this.sendMedicalCardCommand ?? (this.sendMedicalCardCommand = new RelayCommand(() =>
            {
                Messenger.Default.Send<NotificationMessage<MedicalCard>>(
                    new NotificationMessage<MedicalCard>(this.card, "MedicalCard"));
            })); }
        }
    }
}