﻿using System.Windows;
using System.Windows.Controls;
using GalaSoft.MvvmLight.Command;

namespace DesktopApp.Helpers
{
    public class RoutedEventArgsToTextBoxConverter : IEventArgsConverter
    {
        public object Convert(object value, object parameter)
        {
            var args = (RoutedEventArgs) value;

            if (args.OriginalSource is TextBox textBox)
                return textBox;

            return null;
        }
    }
}