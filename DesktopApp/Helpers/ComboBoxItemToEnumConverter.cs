﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Model.Common;

namespace DesktopApp.Helpers
{
    public class ComboBoxItemToEnumConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType.IsEnum)
            {
                ComboBoxItem cbi = value as ComboBoxItem;

                switch (targetType.Name)
                {
                    case "BloodGroup":
                        if (cbi == null) return BloodGroup.None;
                        return (BloodGroup)int.Parse(cbi.Tag.ToString());
                    case "Gender":
                        if (cbi == null) return Gender.None;
                        return (Gender)int.Parse(cbi.Tag.ToString());
                    case "Rhesus":
                        if (cbi == null) return Rhesus.None;
                        return (Rhesus)int.Parse(cbi.Tag.ToString());
                }
            }

            return DependencyProperty.UnsetValue;
        }
    }
}