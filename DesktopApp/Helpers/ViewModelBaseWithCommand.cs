﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace DesktopApp.Helpers
{
    public class ViewModelBaseWithCommand : ViewModelBase
    {
        private RelayCommand<TextBox> selectTextCommand;

        public RelayCommand<TextBox> SelectTextCommand
        {
            get
            {
                return this.selectTextCommand ?? (this.selectTextCommand = new RelayCommand<TextBox>((tb) =>
                {
                    if (tb == null) return;
                    tb.SelectAll();
                }));
            }
        }

        private RelayCommand<MouseButtonEventArgs> onPrewiewMouseLeftButtonDownCommand;

        public RelayCommand<MouseButtonEventArgs> OnPrewiewMouseLeftButtonDownCommand
        {
            get
            {
                return this.onPrewiewMouseLeftButtonDownCommand ??
                       (this.onPrewiewMouseLeftButtonDownCommand = new RelayCommand<MouseButtonEventArgs>((e) =>
                       {

                           DependencyObject parent = e.OriginalSource as UIElement;
                           while (parent != null && !(parent is TextBox))
                               parent = VisualTreeHelper.GetParent(parent);

                           if (parent != null)
                           {
                               var textBox = (TextBox)parent;
                               if (!textBox.IsKeyboardFocusWithin)
                               {
                                   textBox.Focus();
                                   e.Handled = true;
                               }
                           }

                       }));
            }
        }
    }
}