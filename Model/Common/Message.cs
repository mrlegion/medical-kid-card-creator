﻿namespace Model.Common
{
    public class Message
    {
        public string MessageText { get; }

        public MessageType Type { get; }

        public Message(string messageText, MessageType type)
        {
            MessageText = messageText;
            Type = type;
        }
    }
}