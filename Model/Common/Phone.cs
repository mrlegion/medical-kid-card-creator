﻿using GalaSoft.MvvmLight;

namespace Model.Common
{
    public class Phone : ViewModelBase
    {
        private string mobile;

        public const string MobilePropertyName = "Mobile";

        /// <summary>
        /// Получение или установка номера мобильного телефона
        /// </summary>
        public string Mobile
        {
            get => this.mobile;
            set => Set(MobilePropertyName, ref this.mobile, value);
        }
        
        private string home;

        public const string HomePropertyName = "Home";

        /// <summary>
        /// Получение или установка номера домашнего телефона
        /// </summary>
        public string Home
        {
            get => this.home;
            set => Set(HomePropertyName, ref this.home, value);
        }
    }
}
