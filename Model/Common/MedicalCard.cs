﻿using GalaSoft.MvvmLight;

namespace Model.Common
{
    public class MedicalCard : ViewModelBase
    {
        private int numberCard;

        public const string NumberCardPropertyName = "NumberCard";

        /// <summary>
        /// Получение или установка номера медицинской карты
        /// </summary>
        public int NumberCard
        {
            get => this.numberCard;
            set => Set(NumberCardPropertyName, ref this.numberCard, value);
        }

        private int kidArea;

        public const string KidAreaPropertyName = "KidArea";

        /// <summary>
        /// Получение или установка номера участка
        /// </summary>
        public int KidArea
        {
            get => this.kidArea;
            set => Set(KidAreaPropertyName, ref this.kidArea, value);
        }

        private BloodGroup bloodGroup;

        public const string BloodGroupPropertyName = "BloodGroup";

        /// <summary>
        /// Получение или установка номера группы крови
        /// </summary>
        public BloodGroup BloodGroup
        {
            get => this.bloodGroup;
            set => Set(BloodGroupPropertyName, ref this.bloodGroup, value);
        }

        private Rhesus rhesusFactor;

        public const string RhesusFactorPropertyName = "RhesusFactor";

        /// <summary>
        /// Получение или установка резуса-фактора
        /// </summary>
        public Rhesus RhesusFactor
        {
            get => this.rhesusFactor;
            set => Set(RhesusFactorPropertyName, ref this.rhesusFactor, value);
        }

        private string numberInsuranceCertificate;

        public const string NumberInsuranceCertificatePropertyName = "NumberInsuranceCertificate";

        /// <summary>
        /// Получение или установка номера страхового полиса
        /// </summary>
        public string NumberInsuranceCertificate
        {
            get => this.numberInsuranceCertificate;
            set => Set(NumberInsuranceCertificatePropertyName, ref this.numberInsuranceCertificate, value);
        }
    }
}