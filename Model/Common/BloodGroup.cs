﻿namespace Model.Common
{
    public enum BloodGroup
    {
        None   = 0,
        First  = 1,
        Two    = 2,
        Three  = 3,
        Four   = 4
    }
}