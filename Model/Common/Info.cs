﻿namespace Model.Common
{
    public class Info
    {
        /// <summary>
        /// Получение или установка информации о человеке
        /// </summary>
        public Person Person { get; set; }

        /// <summary>
        /// Получение или установка информации об адресе человека
        /// </summary>
        public Address Address { get; set; }

        /// <summary>
        /// Получение или установка информации о телефонах человека
        /// </summary>
        public Phone Phones { get; set; }

        /// <summary>
        /// Получение или установка информации о медецинской карте человека
        /// </summary>
        public MedicalCard Card { get; set; }
    }
}