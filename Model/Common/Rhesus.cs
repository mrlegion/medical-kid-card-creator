﻿namespace Model.Common
{
    public enum Rhesus
    {
        None,
        Positive,
        Negative
    }
}