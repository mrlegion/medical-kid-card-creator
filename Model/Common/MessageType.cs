﻿namespace Model.Common
{
    public enum MessageType
    {
        None,
        Information,
        Warning,
        Error
    }
}