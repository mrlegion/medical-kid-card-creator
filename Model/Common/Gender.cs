﻿namespace Model.Common
{
    public enum Gender
    {
        None  = 0,
        Boy   = 1,
        Girl  = 2
    }
}