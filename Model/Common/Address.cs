﻿using GalaSoft.MvvmLight;

namespace Model.Common
{
    public class Address : ViewModelBase
    {
        private string city = "Новоуральск";

        public const string CityPropertyName = "City";

        /// <summary>
        /// Получение или установка значения имени города
        /// </summary>
        public string City
        {
            get => this.city;
            set => Set(CityPropertyName, ref this.city, value);
        }
        
        private string street;

        public const string StreetPropertyName = "Street";

        /// <summary>
        /// Получение или установка значения улицы
        /// </summary>
        public string Street
        {
            get => this.street;
            set => Set(StreetPropertyName, ref this.street, value);
        }

        private string house;

        public const string HousePropertyName = "House";

        /// <summary>
        /// Получение или установка значения номера дома
        /// </summary>
        public string House
        {
            get => this.house;
            set => Set(HousePropertyName, ref this.house, value);
        }

        private string suffix;

        public const string SuffixPropertyName = "Suffix";

        /// <summary>
        /// Получение или установка значения суффикса дома (может быть пустым)
        /// </summary>
        public string Suffix
        {
            get => this.suffix;
            set => Set(SuffixPropertyName, ref this.suffix, value);
        }

        private string appartment;

        public const string AppartmentPropertyName = "Appartment";

        /// <summary>
        /// Получение или установка значения номера квартиры
        /// </summary>
        public string Appartment
        {
            get => this.appartment;
            set => Set(AppartmentPropertyName, ref this.appartment, value);
        }
    }
}
