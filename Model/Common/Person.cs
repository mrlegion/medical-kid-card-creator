﻿using System;
using GalaSoft.MvvmLight;

namespace Model.Common
{
    public class Person : ViewModelBase
    {
        private string firstName;

        public const string FirstNamePropertyName = "FirstName";

        /// <summary>
        /// Получение или установка имени человека
        /// </summary>
        public string FirstName
        {
            get => this.firstName;
            set => Set(FirstNamePropertyName, ref this.firstName, value);
        }

        private string lastName;

        public const string LastNamePropertyName = "LastName";

        /// <summary>
        /// Получение или установка фамилии человека
        /// </summary>
        public string LastName
        {
            get => this.lastName;
            set => Set(LastNamePropertyName, ref this.lastName, value);
        }

        private string middleName;

        public const string MiddleNamePropertyName = "MiddleName";

        /// <summary>
        /// Получение или установка отчества человека
        /// </summary>
        public string MiddleName
        {
            get => this.middleName;
            set => Set(MiddleNamePropertyName, ref this.middleName, value);
        }

        private DateTime birthday = new DateTime(2010, 01, 01);

        public const string BirthdayPropertyName = "Birthday";

        /// <summary>
        /// Получение или установка даты рождения
        /// </summary>
        public DateTime Birthday
        {
            get => this.birthday;
            set => Set(BirthdayPropertyName, ref this.birthday, value);
        }

        private Gender gender;

        public const string GenderPropertyName = "Gender";

        /// <summary>
        /// Получение или установка пола человека
        /// </summary>
        public Gender Gender
        {
            get => this.gender;
            set => Set(GenderPropertyName, ref this.gender, value);
        }
    }
}