﻿namespace Model.Interface
{
    public interface IPdfEditor
    {
        void PlaceText(string text, float x, float y);
    }
}