﻿using System.IO;
using GalaSoft.MvvmLight.Messaging;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.StyledXmlParser.Node;
using Model.Common;
using Model.Interface;
using Path = System.IO.Path;

namespace Model
{
    public class Creator
    {
        private readonly IPdfEditor editor;
        private PdfDocument document;
        private DirectoryInfo saveDirectory;
        private Info info;

        public Creator(IPdfEditor editor)
        {
            this.editor = editor;
        }

        public void CreatingMedicalCard(Info info)
        {
            this.info = info;
        }

        public bool SetSavePath(string path)
        {
            if (!Directory.Exists(path))
                return false;

            this.saveDirectory = new DirectoryInfo(path);
            return true;
        }

        private string GenerateFileName(string lastname)
        {
            return $"История развития ребенка {lastname ?? ""}.pdf";
        }

        private bool CreateFile()
        {
            var file = Path.Combine(this.saveDirectory.FullName, GenerateFileName(this.info.Person.LastName));

            if (File.Exists(file))
                Messenger.Default.Send<NotificationMessage<Message>>(
                    new NotificationMessage<Message>(
                        new Message($"File exist, {file}", MessageType.Error), "Creator"));

            this.document = new PdfDocument(new PdfWriter(file));
            this.document.SetDefaultPageSize(new PageSize(180, 140));
            return true;
        }

        private void CreateTitle(string number)
        {
        }
    }
}
