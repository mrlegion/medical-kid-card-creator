﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var directory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            PdfTest pdf = new PdfTest(directory);
            pdf.SetDocumentSize(180, 140);
            pdf.AddNewPage();

            // Adding text
            pdf.AddText("458");

            pdf.SavePdf();
            
            // delay
            //Console.WriteLine("Для закрытия приложения нажмите на любую клавишу...");
            //Console.ReadKey();
        }
    }
}
