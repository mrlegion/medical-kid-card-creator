﻿using System;
using System.IO;
using System.Text;
using iText.IO.Font;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using Path = System.IO.Path;

namespace ConsoleApp
{
    public class PdfTest
    {
        private PdfDocument document;
        private FileInfo file;

        private PdfCanvas canvas;
        private PdfPage page;

        private const float pt = 2.835f;
        private const string RegularFontFile = "fonts/charterc-bold.otf";
        private const string BoldFontFile = "fonts/charterc-bold.otf";
        private const string BoldItalicFontFile = "fonts/charterc-bolditalic.otf";
        private const string ItalicFontFile = "fonts/charterc-italic.otf";

        private PdfFont regular;
        private PdfFont bold;
        private PdfFont italic;
        private PdfFont boldItalic;

        public PdfTest(string path)
        {
            if (!Directory.Exists(path))
                throw new DirectoryNotFoundException();

            this.file = new FileInfo(Path.Combine(path, "История развития ребенка.pdf"));
            if (this.file.Exists)
                this.file.Delete();

            this.document = new PdfDocument(new PdfWriter(this.file));

            SetFonts();
        }

        public void SetDocumentSize(float width, float height)
        {
            var size = new PageSize(ConvertToPt(width), ConvertToPt(height));
            this.document.SetDefaultPageSize(size);
        }

        public void AddNewPage()
        {
            this.page = this.document.AddNewPage();
            this.canvas = new PdfCanvas(this.page);
        }

        public void SavePdf()
        {
            this.document.Close();
        }

        public void AddText(string text)
        {
            if (string.IsNullOrEmpty(text))
                throw new ArgumentNullException(nameof(text), "Text can not be null or empty");
            
            SetTitle(text);
            SetInsurancePolicy("1234567890123456");
        }

        private void SetTitle(string number)
        {
            var tRectangle = new Rectangle(ConvertToPt(10f), ConvertToPt(91.7f), ConvertToPt(160f), ConvertToPt(5f));
            var tCanvas = new Canvas(this.canvas, this.document, tRectangle);
            var tText = new Text("История развития ребенка №".ToUpper());

            var tParagraph = new Paragraph()
                .SetFont(this.bold)
                .SetFontSize(18)
                .SetTextAlignment(TextAlignment.CENTER)
                .SetVerticalAlignment(VerticalAlignment.TOP)
                .Add(tText)
                .Add($" {number}");

            tCanvas.Add(tParagraph);
            tCanvas.Close();
        }

        private void SetInsurancePolicy(string number)
        {
            var tRectangle = new Rectangle(ConvertToPt(105.8f), ConvertToPt(121.5f), ConvertToPt(65f), ConvertToPt(15f));
            var tCanvas = new Canvas(this.canvas, this.document, tRectangle);

            var tText1 = new Text("Страховой полис №:")
                .SetFontSize(10);

            var sb = new StringBuilder();
            int count = 0;
            for (int i = 0; i < number.Length; i++)
            {
                if (count == 4)
                {
                    sb.Append(" ");
                    count = 0;
                }

                sb.Append(number[i]);
                count++;

            }

            var tText2 = new Text(sb.ToString())
                .SetFontSize(14)
                .SetFont(this.bold);

            var tParagraph = new Paragraph()
                .SetVerticalAlignment(VerticalAlignment.TOP)
                .SetTextAlignment(TextAlignment.RIGHT)
                .SetFixedLeading(16)
                .SetFont(this.regular)
                .Add(tText1)
                .Add(Environment.NewLine)
                .Add(tText2);

            tCanvas.Add(tParagraph);
            tCanvas.Close();
        }


        private void SetFonts()
        {
            // Regular
            FontProgram fpRegular = FontProgramFactory.CreateFont(RegularFontFile);
            this.regular = PdfFontFactory.CreateFont(fpRegular, "Cp1251", true);

            // Bold
            FontProgram fpBold = FontProgramFactory.CreateFont(BoldFontFile);
            this.bold = PdfFontFactory.CreateFont(fpBold, "Cp1251", true);

            // Italic
            FontProgram fpItalic = FontProgramFactory.CreateFont(ItalicFontFile);
            this.italic = PdfFontFactory.CreateFont(fpItalic, "Cp1251", true);

            // Bold Italic
            FontProgram fpBoldItalic = FontProgramFactory.CreateFont(BoldItalicFontFile);
            this.boldItalic = PdfFontFactory.CreateFont(fpBoldItalic, "Cp1251", true);
        }

        private float ConvertToPt(float value) => value * pt;
    }
}